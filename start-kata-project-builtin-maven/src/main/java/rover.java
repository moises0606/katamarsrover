
public class rover {

	private int x;
	private int y;
	private int dir;

	public rover() {
		super();
		this.x = 0;
		this.y = 0;
		this.dir = 0;
	}

	public void move(String mov[]) {

		for (int i = 0; i < mov.length; i++) {
			switch (mov[i]) {
			case "F":
				moveForward();
				break;
			case "B":
				moveBackward();
				break;
			case "L":
				changeDirection("L");
				break;
			case "R":
				changeDirection("R");
				break;
			}
		}

	}

	public void moveForward() {
		// necesitamos la direccion
		switch (this.dir) {
		case 0:
			this.y += 1;
			break;
		case 1:
			this.x += 1;
			break;
		case 2:
			this.y -= 1;
			break;
		case 3:
			this.x -= 1;
			break;
		default:
			return;
		}
	}

	public void moveBackward() {
		// necesitamos la direccion
		switch (this.dir) {
		case 0:
			this.y -= 1;
			break;
		case 1:
			this.x -= 1;
			break;
		case 2:
			this.y += 1;
			break;
		case 3:
			this.x += 1;
			break;
		default:
			return;
		}
	}

	public void changeDirection(String LorR) { //ENTRA L or R
		int position = this.dir;
		switch (LorR) {
		case "L":
			if (this.dir == 0)
				position = 3;
			else
				position -= 1;
			break;
		case "R":
			if (this.dir == 3)
				position = 0;
			else
				position += 1;
			break;
		default:
			return;

		}
		this.dir = position;
	}

	public String getDirectionString() {
		String Direction = "";

		switch (this.dir) {
		case 0:
			Direction = "N";
			break;
		case 1:
			Direction = "E";
			break;
		case 2:
			Direction = "S";
			break;
		case 3:
			Direction = "W";
			break;
		}
		return Direction;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getDir() {
		return dir;
	}

	public void setDir(int dir) {
		this.dir = dir;
	}

	public String getLocation() {
		String position;

		position = "Y: " + getY() + ",";

		position += " X: " + getX() + ",";

		position += " Dir: " + getDirectionString();

		return position;
	}

}
