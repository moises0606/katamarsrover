import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class roverShould {

	@Test
	public void init_rover_on_mars() {

		rover rover = new rover();

		assertEquals(rover.getX(), 0);
		assertEquals(rover.getY(), 0);
		assertEquals(rover.getDir(), 0);

		System.out.println("rover.getX(): " + rover.getX());
		System.out.println("rover.getY(): " + rover.getY());
		System.out.println("rover.getDir(): " + rover.getDir());

	}

	@Test
	public void move_north() {
		rover rover = new rover();
		rover.moveForward();
		assertEquals(rover.getY(), 1);
		rover.moveBackward();
		assertEquals(rover.getY(), 0);
	}

	@Test
	public void move_east() {
		rover rover = new rover();

		rover.changeDirection("R");
		assertEquals(rover.getDirectionString(), "E");
		rover.moveForward();
		assertEquals(rover.getX(), 1);
		rover.moveBackward();
		assertEquals(rover.getX(), 0);
	}

	@Test
	public void move_south() {
		rover rover = new rover();

		rover.changeDirection("R");
		rover.changeDirection("R");
		assertEquals(rover.getDirectionString(), "S");
		rover.moveForward();
		assertEquals(rover.getY(), -1);
		rover.moveBackward();
		assertEquals(rover.getY(), 0);
	}

	@Test
	public void move_west() {
		rover rover = new rover();

		rover.changeDirection("L");
		assertEquals(rover.getDirectionString(), "W");
		rover.moveForward();
		assertEquals(rover.getX(), -1);
		rover.moveBackward();
		assertEquals(rover.getX(), 0);
	}

	@Test
	public void move_left_or_rigth() {
		rover rover = new rover();
		
		rover.changeDirection("L");
		assertEquals(rover.getDir(), 3);
		rover.changeDirection("R");
		rover.changeDirection("R");
		assertEquals(rover.getDir(), 1);
	}

	@Test
	public void move_should() {
		rover rover = new rover();
//		String[] driveroveralongmars = { "B", "F", "B", "L", "B" };
		String[] driveroveralongmars = { "R" };
		rover.move(driveroveralongmars);
		System.out.println(rover.getLocation());

	}

}
